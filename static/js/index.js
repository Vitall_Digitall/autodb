var app = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data: {
        actions: {data: {allActions: {edges: []}}},
        regNum: ""
    },
    methods: {
        fetchQuery() {
            axios.post('/graphql', {query: 'query data {allActions(last:10, regNum:"'+ this.regNum +'", orderBy: "operationDate") {edges {node {regNum capacity makeYear car {model brand {name}} fuel {name} color {color} operation{ name code} operationDate }}}}'}).then(response => {this.actions = response.data});
        }
    } 
})