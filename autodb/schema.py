import graphene
import autodb.auto.schema


class Query(autodb.auto.schema.Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query)
