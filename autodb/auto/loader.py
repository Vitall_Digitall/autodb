import csv
from autodb.auto.models import Operation, Brand, BrandModel, Fuel, Color, VehicleKind, Action


def sync_operation(row):
    code = int(row['oper_code'])
    name = row['oper_name'].replace(f"{code} - ", "").strip()
    defaults = {'name': name}
    operation, created = Operation.objects.get_or_create(code=code, defaults=defaults)
    return operation


def sync_color(row):
    color, created = Color.objects.get_or_create(color=row['color'])
    return color


def sync_brand_and_model(row):
    model = row['model']
    brand = row['brand'].replace(model, '').strip()
    brand_obj, created = Brand.objects.get_or_create(name=brand)
    model, created = BrandModel.objects.get_or_create(model=model, brand=brand_obj)
    return model


def sync_fuel(row):
    fuel, created = Fuel.objects.get_or_create(name=row['fuel'])
    return fuel


def sync_vehicle_kind(row):
    vehicle, created = VehicleKind.objects.get_or_create(kind=row['kind'])
    return vehicle


with open('tz_opendata_z01012013_po31122013.csv', 'r') as f:
    reader = csv.DictReader(f, delimiter=';')
    for row in reader:
        operation = sync_operation(row)
        color = sync_color(row)
        model = sync_brand_and_model(row)
        fuel = sync_fuel(row)
        kind = sync_vehicle_kind(row)
        body_list = row['body'].split('-')
        kwargs = {
            'car': model,
            'color': color,
            'operation': operation,
            'operation_date': row['d_reg'],
            'fuel': fuel,
            'make_year': row['make_year'],
            'body': '-'.join(body_list[:-1]),
            'license_category': body_list[-1],
        }
        if row['reg_addr_koatuu'] != 'NULL':
            kwargs['koatuu'] = int(row['reg_addr_koatuu'])
        if row['capacity'] != 'NULL':
            kwargs['capacity'] = int(row['capacity'])
        if 'birthday' in row and row['birthday'] != 'NULL':
            kwargs['owner_birthday'] = row['birthday']
        if 'inn_char' in row and row['inn_char'] != 'NULL':
            kwargs['inn'] = row['inn_char']

        Action.objects.create(reg_num=row['n_reg_new'], **kwargs)
        print(f"{row['n_reg_new']} - {row['d_reg']}")
