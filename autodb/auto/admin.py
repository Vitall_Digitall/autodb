from django.contrib import admin
from autodb.auto.models import (
    Brand, BrandModel, Fuel, VehicleKind, Color, Operation, Action
)

# Register your models here.

admin.site.register(Brand)
admin.site.register(BrandModel)
admin.site.register(Fuel)
admin.site.register(VehicleKind)
admin.site.register(Color)
admin.site.register(Operation)
admin.site.register(Action)
