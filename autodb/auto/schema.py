import django_filters
import graphene
from graphene import relay, ObjectType
from django_filters import OrderingFilter, DateFilter
from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from autodb.auto.models import Operation, Color, VehicleKind, Action, Fuel, Brand, BrandModel


class CountableConnectionBase(relay.Connection):
	class Meta:
		abstract = True

	total_count = graphene.Int()
	def resolve_total_count(self, info, **kwargs):
		return self.iterable.count()


class OperationFilter(django_filters.FilterSet):
    class Meta:
        model = Operation
        fields = {
            'code': ['exact'],
            'operations__operation_date': ['exact']
        }


class OperationNode(DjangoObjectType):
    class Meta:
        model = Operation
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase


class ColorType(DjangoObjectType):
    class Meta:
        model = Color
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase


class VehicleKindType(DjangoObjectType):
    class Meta:
        model = VehicleKind


class FuelTypeFilter(django_filters.FilterSet):
    class Meta:
        model = Fuel
        fields = ['id']


class FuelType(DjangoObjectType):
    class Meta:
        model = Fuel
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase


class BrandType(DjangoObjectType):
    class Meta:
        model = Brand
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase

class BrandModelType(DjangoObjectType):
    class Meta:
        model = BrandModel
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase

class ActionFilter(django_filters.FilterSet):
    class Meta:
        model = Action
        fields = {
            'id': ['exact'],
            'reg_num': ['exact', 'icontains', 'istartswith'],
            'inn': ['exact'],
            'owner_birthday': ['exact'],
            'operation_date': ['exact', 'gte', 'lte'],
            'operation__code': ['exact'],
        }
    order_by = OrderingFilter(fields=('operation_date',))
    operation_date = DateFilter()


class ActionNode(DjangoObjectType):
    class Meta:
        model = Action
        interfaces = (relay.Node,)
        connection_class = CountableConnectionBase


class Query(object):
    all_colors = graphene.List(ColorType)
    operation = relay.Node.Field(OperationNode)
    all_operations = DjangoFilterConnectionField(OperationNode, filterset_class=OperationFilter)
    all_fuels = graphene.List(FuelType)
    all_vehiclekinds = graphene.List(VehicleKindType)
    all_brands = graphene.List(BrandType)
    all_models = graphene.List(BrandModelType)
    model = graphene.Field(BrandModelType, id=graphene.Int(), brand=graphene.String(), brand_id=graphene.Int())
    brand = graphene.Field(BrandType, id=graphene.Int(), name=graphene.String())
    all_actions = DjangoFilterConnectionField(ActionNode, filterset_class=ActionFilter)

    # reg_number = graphene.List(ActionType, reg_num=graphene.String(), operation_date=graphene.String())

    def resolve_all_colors(self, info, **kwargs):
        return Color.objects.all()

    # def resolve_all_operations(self, info, **kwargs):
    #     return Operation.objects.all()

    def resolve_all_fuels(self, info, **kwargs):
        return Fuel.objects.all()

    def resolve_all_brands(self, info, **kwargs):
        return Brand.objects.all()

    def resolve_all_models(self, info, **kwargs):
        return BrandModel.objects.select_related('brand').all()

    def resolve_model(self, info, **kwargs):
        mid = kwargs.get('id')
        brand = kwargs.get('brand')
        brand_id = kwargs.get('brand_id')

        if mid is not None:
            return BrandModel.objects.get(pk=mid)

        if brand is not None:
            return BrandModel.objects.get(brand__name=brand)

        if brand_id is not None:
            return BrandModel.objects.get(brand_id=brand_id)

        return None

    def resolve_brand(self, info, **kwargs):
        bid = kwargs.get('id')
        name = kwargs.get('name')

        if bid:
            return Brand.objects.get(pk=bid)
        if name:
            return Brand.objects.get(name=name)
        return None

    # def resolve_reg_number(self, info, **kwargs):
    #     reg_num = kwargs.get('reg_num')
    #     operation_date = kwargs.get('operation_date')
    #     if reg_num is not None:
    #         return Action.objects.filter(reg_num=reg_num)
    #     if operation_date is not None:
    #         return Action.objects.filter(operation_date=operation_date)

    #     return None
