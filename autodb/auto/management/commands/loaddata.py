# -*- coding: utf-8 -*-
import csv
from django.core.management.base import BaseCommand

from autodb.auto.models import Action
from autodb.auto.management.commands._tools import (
    sync_brand_and_model, sync_color, sync_fuel, sync_operation, sync_vehicle_kind, printProgressBar
)


class Command(BaseCommand):
    help = "Command for import csv data to db"

    def handle(self, *args, **options):
        file_path = options['file']
        start_date = options['date']
        with open(file_path, 'r') as f:
            print('Calculating...')
            reader = csv.DictReader(f, delimiter=';')
            rows_count = sum(1 for row in reader)
            printProgressBar(0, rows_count, prefix='Progress:', suffix='Complete', length=50)
            i = 1
            f.seek(0)
            reader.__next__()
            for row in reader:
                operation_date = row['d_reg']
                if start_date and start_date > operation_date:
                    printProgressBar(i, rows_count, prefix='Progress:', suffix='Complete', length=50)
                    i += 1
                    continue
                operation = sync_operation(row)
                color = sync_color(row)
                model = sync_brand_and_model(row)
                fuel = sync_fuel(row)
                kind = sync_vehicle_kind(row)
                body_list = row['body'].split('-')
                kwargs = {
                    'car': model,
                    'color': color,
                    'operation': operation,
                    'operation_date': operation_date,
                    'fuel': fuel,
                    'make_year': row['make_year'],
                    'body': '-'.join(body_list[:-1]),
                    'license_category': body_list[-1],
                }
                if row['reg_addr_koatuu'] != 'NULL':
                    kwargs['koatuu'] = int(row['reg_addr_koatuu'])
                if row['capacity'] != 'NULL':
                    kwargs['capacity'] = int(row['capacity'])
                if 'birthday' in row and row['birthday'] != 'NULL':
                    kwargs['owner_birthday'] = row['birthday']
                if 'inn_char' in row and row['inn_char'] != 'NULL':
                    kwargs['inn'] = row['inn_char']

                Action.objects.create(reg_num=row['n_reg_new'], **kwargs)
                i += 1
                printProgressBar(i, rows_count, prefix='Progress:', suffix='Complete', length=50)
                # print(f"{row['n_reg_new']} - {operation_date}")

    def add_arguments(self, parser):
        parser.add_argument('-f', '--file', required=True, help='Path to file with data')
        parser.add_argument('-d', '--date', default=None, help='Date from will be started import (YYYY-MM-DD)')
