# -*- coding: utf-8 -*-
from autodb.auto.models import Operation, Brand, BrandModel, Fuel, Color, VehicleKind


def sync_operation(row):
    code = int(row['oper_code'])
    name = row['oper_name'].replace(f"{code} - ", "").strip()
    defaults = {'name': name}
    operation, created = Operation.objects.get_or_create(code=code, defaults=defaults)
    return operation


def sync_color(row):
    color, created = Color.objects.get_or_create(color=row['color'])
    return color


def sync_brand_and_model(row):
    model = row['model']
    brand = row['brand'].replace(model, '').strip()
    brand_obj, created = Brand.objects.get_or_create(name=brand)
    model, created = BrandModel.objects.get_or_create(model=model, brand=brand_obj)
    return model


def sync_fuel(row):
    fuel, created = Fuel.objects.get_or_create(name=row['fuel'])
    return fuel


def sync_vehicle_kind(row):
    vehicle, created = VehicleKind.objects.get_or_create(kind=row['kind'])
    return vehicle


def printProgressBar (iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()
