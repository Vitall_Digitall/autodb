from django.db import models

# Create your models here.


class Operation(models.Model):
    code = models.IntegerField()
    name = models.TextField()

    def __str__(self):
        return self.name


class Brand(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class BrandModel(models.Model):
    model = models.CharField(max_length=100)
    brand = models.ForeignKey('Brand', related_name='models', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Fuel(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class VehicleKind(models.Model):
    kind = models.CharField(max_length=50)

    def __str__(self):
        return self.kind


class Color(models.Model):
    color = models.CharField(max_length=50)

    def __str__(self):
        return self.color


class Action(models.Model):
    reg_num = models.CharField(max_length=16, blank=True, null=True)
    car = models.ForeignKey('BrandModel', on_delete=models.CASCADE)
    color = models.ForeignKey('Color', on_delete=models.CASCADE)
    make_year = models.IntegerField()
    capacity = models.IntegerField(blank=True, null=True)
    operation = models.ForeignKey('Operation', related_name='operations', on_delete=models.CASCADE)
    fuel = models.ForeignKey('Fuel', on_delete=models.CASCADE)
    operation_date = models.DateField()
    koatuu = models.FloatField(blank=True, null=True)
    body = models.CharField(max_length=50)
    license_category = models.CharField(max_length=50)
    owner_birthday = models.DateField(blank=True, null=True)
    inn = models.FloatField(blank=True, null=True)
